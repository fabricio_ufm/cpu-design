import memory1
import control_unit1
import os
import re
import time
import yaml

# clase RAM
class Ram:
    def __init__(self,ram1):
        self.ram1 = ram1 

# Class for ALU    
class Alu(control_unit1.Ic):

    # Initializer
    def __init__(self, zero, overflow, negative):
        self.zero = zero
        self.negative = negative
        self.overflow = overflow
        #self.upcode = upcode


    # Instance method OUTPUT
    def output(self, location):
        memory1.registros.OR = ram1.ram1[location]
        return print(memory1.registros.OR)

    # Instance method READ INTO A
    def read_intoA (self, location):
        memory1.registros.a = ram1.ram1[location]
        return memory1.registros.a
    
    # Instance method READ INTO B
    def read_intoB (self, location):
        memory1.registros.b = ram1.ram1[location]
        return memory1.registros.b

    # Instance method AND
    def AND_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)
    
        if pair_int[0]==0 and pair_int[1] == 0:
            and_result = memory1.registros.a & memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 0:
            and_result = memory1.registros.a & memory1.registros.b
        if pair_int[0]==0 and pair_int[1] == 1:
            and_result = memory1.registros.a & memory1.registros.b
        if pair_int[0]==1 and pair_int[1] == 1:
            and_result = memory1.registros.b & memory1.registros.b

        return print(and_result)
        

    # Instance method READ CONSTANT INTO REGISTER A
    def Read_consA(self, constant):
        memory1.registros.a = constant
        return memory1.registros.a

    # Instance method REGISTER A INTO RAM LOCATION
    def AtoRAM(self, location):
        memory1.registros.a = ram1.ram1[location]
        return memory1.registros.a
    
    # Instance method REGISTER B INTO RAM LOCATION
    def BtoRAM(self, location):
        memory1.registros.b = ram1.ram1[location]
        return memory1.registros.b

    # Instance method OR
    def OR_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)
    
        if pair_int[0]==0 and pair_int[1] == 0:
            or_result = memory1.registros.a | memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 0:
            or_result = memory1.registros.a | memory1.registros.b
        if pair_int[0]==0 and pair_int[1] == 1:
            or_result = memory1.registros.a | memory1.registros.b
        if pair_int[0]==1 and pair_int[1] == 1:
            or_result = memory1.registros.b | memory1.registros.b
        return print(or_result)

    # Instance method READ CONSTANT INTO REGISTER B
    def Read_consB(self, constant):
        memory1.registros.b = constant
        return memory1.registros.b

    # Instance method SUM/ADD
    def SUM_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)
    
        if pair_int[0]==0 and pair_int[1] == 0:
            sum_result = memory1.registros.a + memory1.registros.a
        elif pair_int[0]==1 and pair_int[1] == 0:
            sum_result = memory1.registros.a + memory1.registros.b
        elif pair_int[0]==0 and pair_int[1] == 1:
            sum_result = memory1.registros.a + memory1.registros.b
        elif pair_int[0]==1 and pair_int[1] == 1:
            sum_result = memory1.registros.b + memory1.registros.b
            if sum_result == 0:
                Alu.zero = True
            elif sum_result >= 16:
                Alu.overflow = True

        return print (sum_result)

    #Instance method SUB
    def SUB_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)
    
        if pair_int[0]==0 and pair_int[1] == 0:
            sub_result = memory1.registros.a - memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 0:
            sub_result = memory1.registros.a - memory1.registros.b
        if pair_int[0]==0 and pair_int[1] == 1:
            sub_result = memory1.registros.a - memory1.registros.b
        if pair_int[0]==1 and pair_int[1] == 1:
            sub_result = memory1.registros.b - memory1.registros.b
            if sub_result == 0:
                Alu.zero = True
            elif sub_result >= 16:
                Alu.overflow = True
            elif sub_result < 0:
                Alu.negative = True
        return print(sub_result)

    def Mayor_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)   
        if pair_int[0]==0 and pair_int[1] == 0:
            may_result = memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 0:
            if memory1.registros.a > memory1.registros.b:
                may_result = memory1.registros.a
            if memory1.registros.b > memory1.registros.a:
                may_result = memory1.registros.b
            else:
                may_result = memory1.registros.a
        if pair_int[0]==0 and pair_int[1] == 1:
            if memory1.registros.a > memory1.registros.b:
                may_result = memory1.registros.a
            if memory1.registros.b > memory1.registros.a:
                may_result = memory1.registros.b
            else:
                may_result = memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 1:
            may_result = memory1.registros.b
        return may_result

    def Mult_operator(self, pair):
        pair_list = []
        pair_int = []
        pair_list=([pair[i:i+2] for i in range(0, len(pair), 2)])
        for i in pair_list:
            var = int(i,2)
            pair_int.append(var)
    
        if pair_int[0]==0 and pair_int[1] == 0:
            mult_result = memory1.registros.a * memory1.registros.a
        if pair_int[0]==1 and pair_int[1] == 0:
            mult_result = memory1.registros.a * memory1.registros.b
        if pair_int[0]==0 and pair_int[1] == 1:
            mult_result = memory1.registros.a * memory1.registros.b
        if pair_int[0]==1 and pair_int[1] == 1:
            mult_result = memory1.registros.b * memory1.registros.b
        return mult_result
    
    
# Instanciate the ALU object
alu_attributes = Alu("", "", "")


# Class for CU
class Cu(control_unit1.Ic):
    
    def __init__(self,clock, code_file):
        self.clock = clock
        self.code_file = code_file
    
    # Instance method binario
    def separar_str(self, binario):
        global lista_binarios
        lista_binarios = ([binario[i:i+4] for i in range(0, len(binario), 4)])
    

    # Instance method leer archivo
    #SI EL NOMBRE DEL ARCHIVO NECESITA CAMBIAR -> 
    def leer_file(self, code_file):
        global lista_completa
        lista_completa = []
        folder = os.path.dirname(os.path.abspath(__file__))
        self.code_file = os.path.join(folder, self.code_file)
        if self.code_file.endswith(".code"):
            with open(self.code_file) as coded:
                cont = coded.readlines()
                for i in cont:
                    linea = i.replace("\n","")
                    if not i.startswith("#"):
                            # print("pasa x aqui")
                            if re.match('^[a-zA-Z]+', linea):
                                # print("tambien aqui")
                                mnemo = re.match("([a-z_]+)([0-9]+)", linea, re.I)
                                # print (mnemo)
                                if mnemo:
                                    items = mnemo.groups()
                                    # print(items)
                                    for tup in items:
                                        # print(tup)
                                        lista_completa.append(tup)
                                        #print(lista_completa, "lista completa funcion leer file")
                            else:
                                lista_completa.append(linea)
        elif self.code_file.endswith(".yml"):
            with open(code_file) as yml:
                lista_completa = yaml.load(yml,Loader=yaml.FullLoader)
        return lista_completa
                
    

    # Instance method separar los bits
    def listaf(self, lista_completa):
        global lista_full
        global mnemonic
        global codeandfile
        mnemonic = ["output", "ld_a", "ld_b", "and", "ild_a", "str_a", "str_b", "or", "ild_b", "add", "sub", "jmp", "jmp_n", "halt"]
        #FALTAN "add", "sub", "jmp", "jmp_n", "halt"
        lista_full = []
        for each in lista_completa:
            if each in mnemonic:
                lista_full.append(each)
            else:
                codeandfile.separar_str(each)
                for i in range(len(lista_binarios)):
                    lista_full.append(lista_binarios[i])
    
        return lista_full
    
    # Instance method rango
    def ran(self, lista_full):
        global rango
        global enteros
        enteros =[]
        for cod in lista_full:
            # print(cod, "i del ran")
            if cod in mnemonic:
                for nemo in mnemonic:
                    if cod == nemo:
                        num = mnemonic.index(nemo)
            else:
                num = int(cod,2)
                #print(num, "num")
            enteros.append(num)
            # print(enteros, "enteros")
        rango = int(len(enteros)/2)
        return rango
        
    # Instance method funciones del alu
    def func(self,rango):
        indice = 0
        dire = 1

        for i in range(rango):
            if  enteros[indice] == 0:
                direc = int(enteros[dire])
                ret = alu_attributes.output(direc)

            if enteros[indice]  == 1:
                direc = int(enteros[dire])
                ret = alu_attributes.read_intoA(direc)
                print(ret)

            if enteros[indice]  == 2:
                direc = int(enteros[dire])
                ret = alu_attributes.read_intoB(direc)

            if enteros[indice] == 3:
                direc = (lista_full[dire])
                ret = alu_attributes.AND_operator(direc)

            if enteros[indice] == 4:
                direc = int(enteros[dire])
                ret = alu_attributes.Read_consA(direc)

            if enteros[indice] == 5:
                direc = int(enteros[dire])
                ret = alu_attributes.AtoRAM(direc)

            if enteros[indice] == 6:
                direc = int(enteros[dire])
                ret = alu_attributes.BtoRAM(direc)

            if enteros[indice] == 7:
                direc = (lista_full[dire])
                ret = alu_attributes.OR_operator(direc)

            if enteros[indice] == 8:
                direc = int(enteros[dire])
                ret = alu_attributes.Read_consB(direc)
            
            if enteros[indice] == 9:
                direc = (lista_full[dire])
                ret = alu_attributes.SUM_operator(dire)

            if enteros[indice] == 10:
                direc = (lista_full[dire])
                ret = alu_attributes.SUB_operator(direc)

            if enteros[indice] == 11:
                direc = enteros[dire]
                indice = direc
                dire = direc +1

            # if enteros[indice] == 12:
            #     direc = int(enteros[dire])
            #     JUMPPPPP_N

            if enteros[indice] == 13:
                direc = lista_full[dire]
                ret = alu_attributes.Mayor_operator(direc)

            if enteros[indice] == 14:
                direc = lista_full[dire]
                ret = alu_attributes.Mult_operator(direc)

            if enteros[indice] == 15:
                ret = input("press enter to exit")

            if type(self.clock) == str:
                ret = input("press enter for next instruction...") 
                
            else:
                ret = time.sleep(self.clock)        
            if enteros[indice] == 11:
                direc = enteros[dire]
                indice = direc
                dire = direc +1
            else:
                indice = indice +2
                dire = dire + 2
            
            if memory1.registros.b == 0:
                alu_attributes.zero = "true"
                ret = alu_attributes.zero               
            if memory1.registros.b < 0 :
                alu_attributes.negative = "true"
                ret = alu_attributes.negative
            if memory1.registros.b > 15 :               
                alu_attributes.overflow = "true"
                ret = alu_attributes.overflow

        return ret

#yaml load
yamlfile = "bios.yml"
yamlconfig = Cu("0",yamlfile)
yam1 = yamlconfig.leer_file(yamlfile)
yam2 = yam1['ram']
yam3 = yam1['clock']
lista_yaml = [yam2['0'],yam2['1'],yam2['2'],yam2['3'],yam2['4'],yam2['5'],yam2['6'],yam2['7'],yam2['8'],yam2['9'],yam2['10'],yam2['11'],yam2['12'],yam2['13'],yam2['14'],yam2['15']]
ram1 = Ram(lista_yaml)
code_file = input("Ingrese doc ")#.code o .yml

# Instance the CU object
codeandfile = Cu(yam3["speed"], code_file)

# Instance methods
code = codeandfile.leer_file(code_file)
code2 = codeandfile.listaf(lista_completa)
code3 = codeandfile.ran(lista_full)
code4 = codeandfile.func(rango)